﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
//using FluentEmail.Core;
//using FluentEmail.Smtp;
using mysqltest.Models;
using Owlapp.Tools;

namespace Owlapp.Controllers
{
    public class AccountController : Controller
    {
        owldbEntities owldb = new owldbEntities();

        public object EnableSsl { get; private set; }

        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Registration()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Registration(user u)
        {
            var secretKey = ConfigurationManager.AppSettings["SecretKey"];

            

                u.password = Seguridad.EncryptString(secretKey, u.password);
                owldb.users.Add(u);
                owldb.SaveChanges();
            
           
            //var sender = new SmtpSender(() => new SmtpClient("smtp.gmail.com")
            //{
            //    UseDefaultCredentials = false,
            //    Port = 587,
            //    Credentials = new NetworkCredential("frr9724@gmail.com", "Saprissa@36"),
            //    EnableSsl = true
            //});

            //Email.DefaultSender = sender;

            //var email = Email
            //    .From("frr9724@gmail.com", "PC")
            //    .To("frr9724@gmail.com", "Francisco Rivera")
            //    .Subject("Testing Owl Learning")
            //    .Body("This is an example plain text");

            //try
            //{
            //    email.SendAsync();
            //}
            //catch (Exception e)
            //{

            //}

            return View();
        }


        //public ActionResult Activation()
        //{
        //    ViewBag.Message = "Invalid Activation code.";
        //    if (RouteData.Values["id"] != null)
        //    {
        //        Guid activationCode = new Guid(RouteData.Values["id"].ToString());
        //        owldbEntities owldb = new owldbEntities();
        //        pass_verif_code pvf = owldbEntities.pass_verif_code.Where(p => p.verification_code = activationCode).FirstOrDefault();
        //        if (userActivation != null)
        //        {
        //            usersEntities.UserActivations.Remove(userActivation);
        //            usersEntities.SaveChanges();
        //            ViewBag.Message = "Activation successful.";
        //        }
        //    }

        //    return View();
        //}


        //private void SendActivationEmail(user user) {

        //    Guid activationCode = Guid.NewGuid();
        //    owldbEntities owldb = new owldbEntities();
        //    owldbEntities.UserActivations.Add(new UserActivation
        //    {
        //        UserId = user.UserId,
        //        ActivationCode = activationCode
        //    });
        //    usersEntities.SaveChanges();


        //}










        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(user u)
        {
            String message = String.Empty;

            var secretKey = ConfigurationManager.AppSettings["SecretKey"];

            var encryptedPassword = Seguridad.EncryptString(secretKey, u.password);

            var lg = owldb.users.Where(a => a.email.Equals(u.email) && a.password.Equals(encryptedPassword)).FirstOrDefault();

            if (lg != null)
            {
                

                return RedirectToAction("Home,Index,Home");
                
            }
            else
            {
                ModelState.AddModelError("","Correo o Clave incorrecto.");
                return View("Login");
            }
         
        }

        // GET: Account/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Account/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Account/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Account/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Account/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Account/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Account/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
