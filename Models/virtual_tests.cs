//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace mysqltest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class virtual_tests
    {
        public virtual_tests()
        {
            this.module_vt = new HashSet<module_vt>();
            this.question_vt = new HashSet<question_vt>();
        }
    
        public int virtual_test_id { get; set; }
        public string name { get; set; }
        public int duration { get; set; }
        public string instructions { get; set; }
        public int type { get; set; }
    
        public virtual ICollection<module_vt> module_vt { get; set; }
        public virtual ICollection<question_vt> question_vt { get; set; }
        public virtual test_type test_type { get; set; }
    }
}
